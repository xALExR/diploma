import matplotlib
import tkinter as tk
import matplotlib.animation as animation
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from tkinter import ttk
from matplotlib import style
style.use('ggplot')
matplotlib.use("TkAgg")

LARGE_FONT = ("Verdana", 12)
f = Figure(figsize=(5, 5), dpi=100)
a = f.add_subplot(111)
x_errors, y_errors = [], []


def animate(i):
    a.clear()
    a.plot_date(x_errors, y_errors, "#00A3E0", label="errors")
    a.legend(bbox_to_anchor=(0, 1.02, 1, .102), loc=3,
             ncol=2, borderaxespad=0)

    title = "Errors"
    a.set_title(title)


class Diploma(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        tk.Tk.wm_title(self, "Diploma")
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        # for F in (StartPage, PageOne, PageTwo, PageThree):
        #     frame = F(container, self)
        #
        #     self.frames[F] = frame
        frame = StartPage(container, self)
        frame.grid(row=0, column=0, sticky="nsew")
        self.frames[StartPage] = frame

        self.show_frame(StartPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

    def learn(self):
        pass

    def calculate(self, *data):
        input_data = data[0].split(',')


class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = ttk.Label(self, text="Введіть дані", font=LARGE_FONT)
        entry = ttk.Entry(self)
        button1 = ttk.Button(self, text="Розрахувати",
                            command=lambda: controller.calculate(entry.get()))
        button2 = ttk.Button(self, text="Навчати",
                            command=lambda: controller.learn)
        label.pack(pady=10, padx=10, side=tk.TOP, expand=1)
        entry.pack(pady=10, padx=10, side=tk.TOP)
        button1.pack(pady=10, padx=10)
        button2.pack(pady=10, padx=10)

        # a.plot([1, 2, 3, 4, 5, 6, 7, 8], [5, 6, 1, 3, 8, 9, 3, 5])

        canvas = FigureCanvasTkAgg(f, self)
        canvas.show()
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.X, expand=True)

        toolbar = NavigationToolbar2TkAgg(canvas, self)
        toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP, fill=tk.X, expand=True)


if __name__ == '__main__':
    app = Diploma()
    app.geometry("900x700")
    ani = animation.FuncAnimation(f, animate, interval=1000)
    app.mainloop()
