# -*- coding: utf-8 -*-
import numpy as np
import itertools as it


class Neural_network:
    def __init__(self):
        self.syn0, self.syn1, self.syn2, self.syn3, self.data, self.result = None, None, None, None, None, None
        self.errors = []
        self.etalon_data = None
        self.count = 0
        self.epoch = None
        self.learn_speed = 0.01
        self.file = None
        self.file_data = []
        self.file_count = 0

    def add_etalon_data(self, data):
        self.etalon_data = data

    def initialize_data(self, data):
        if data is '' and self.file is None:
            raise NotImplementedError("Вхідні данні не задані.\nПриклад даних:\n0.123, 0.53, 0.98")
        elif self.file is not None:
            for i in self.file:
                i = i[20:].split()
                dt = []
                for j in i:
                    dt.append(float(j))
                self.file_data.append(dt)
            # self.file_data = [int(i.split(',')) for i in self.file]
            print(self.file_data[-1])
            self.data = self.file_data[0]
        else:
            input_data = data.split(',')
            self.data = [float(i) for i in input_data]

    def initialize_syn(self):
        lenght = len(self.data)
        from math import factorial
        comb = int(factorial(lenght*3) / (factorial(2) * factorial(lenght*3-2)))
        self.syn0 = [np.random.random(2) for i in range(comb)]
        self.syn1 = [np.random.random() for j in range(comb)]
        self.syn2 = [np.random.random() for l in range(comb)]
        self.syn3 = [np.random.random() for t in range(comb)]

    def initialize_epoch(self, value):
        if self.file is not None:
            self.epoch = len(self.file)
        else:
            self.epoch = int(value)

    def gbellmf(self, x, a, b, c):
        return 1. / (1. + np.abs((x - c) / a) ** (2 * b))

    def sigm(self, x, deriv=False):
        if deriv is True:
            return x * (1 - x)
        return 1 / (1 + np.exp(-x))

    def A_B_C_random(self):
        return np.random.triangular(0.2, 0.6, 0.8, 3)

    def generate_terms(self, x):
        res = []
        for i in x:
            res += [i, i, i]
        return res

    def func_belongs(self, lo):
        # A, B, C = self.A_B_C_random()
        A, B, C = 0.2, 0.6, 0.4
        terms = np.array(self.generate_terms(lo))
        # return 1 / (1 + abs(terms - C / A) ** (2 * B))
        # res = []
        # for i in terms:
        #     A, B, C = A_B_C_random()
        #     res.append((1/(1+abs(i-C/A)**(2*B))))
        return self.gbellmf(terms, a=A, b=B, c=C)

    def func_rules(self, l1):
        res = []
        count = 0
        for i in it.combinations(l1, 2):
            res.append((i[0] * self.syn0[count][0]) * (i[1] * self.syn0[count][1]))
            count += 1
        return np.array(res)

    def notmalize(self, l2):
        l2 *= self.syn1
        sum = l2.sum()
        norm = np.array([i / sum for i in l2])
        return norm

    def layer_four(self, l2, l3):
        l3 *= self.syn2
        return np.multiply(l2, l3)

    def layer_five(self, l4):
        l4 *= self.syn3
        return l4.sum()

    def __iter__(self):
        if self.syn0 is None or self.syn1 is None or self.syn2 is None or self.syn3 is None:
            raise NotImplementedError("Звязки не задані.\nСпочатку виконайте\nнавчання мережі")
        return self

    def __next__(self):
        if self.epoch > 0:
            if self.file is not None:
                l0 = self.file_data[self.file_count]
                self.file_count += 1
            else:
                l0 = self.data
            l1 = self.func_belongs(l0)
            l2 = self.func_rules(l1)
            l3 = self.notmalize(l2)
            l4 = self.layer_four(l2, l3)
            l5 = self.layer_five(l4)
            self.result = l5
            if self.etalon_data is not None:
                # if self.count is not 0 and self.errors[-1] == 0:
                #     raise KeyError()
                self.errors.append(l5 - self.etalon_data)
                self.learn_syn(l5, l4=l4, l3=l3, l2=l2, l1=l1)
            self.count += 1
            self.epoch -= 1
        else:
            raise StopIteration()

    def learn_syn(self, out, l4, l3, l2, **l1):
        l1 = l1['l1']
        delta5 = out*(1-out)*(self.etalon_data-out)
        # print("delt5", delta5)
        syn3 = []
        for s3 in range(len(self.syn3)):
            syn3.append(self.syn3[s3] + self.learn_speed*delta5*l4[s3])
        self.syn3 = syn3
        # print("self.syn3", self.syn3)

        delta4 = [l4[i]*(1-l4[i])*delta5*self.syn3[i] for i in range(len(l4))]
        syn2 = []
        for s2 in range(len(self.syn2)):
            syn2.append(self.syn2[s2] + self.learn_speed * delta4[s2] * l3[s2])
        self.syn2 = syn2
        # print("self.syn2", self.syn2)

        delta3 = [l3[i] * (1 - l3[i]) * delta4[i] * self.syn2[i] for i in range(len(l3))]
        syn1 = []
        for s1 in range(len(self.syn1)):
            syn1.append(self.syn1[s1] + self.learn_speed * delta3[s1] * l2[s1])
        self.syn1 = syn1
        # print("self.syn1", self.syn1)

        delta2 = [l2[i] * (1 - l2[i]) * delta3[i] * self.syn1[i] for i in range(len(l2))]
        syn0 = []
        l1 = [i for i in it.combinations(l1, 2)]
        for s1 in range(len(self.syn0)):
            syn0.append([self.syn0[s1][0] + self.learn_speed * delta2[s1] * l1[s1][0], self.syn0[s1][1] + self.learn_speed * delta2[s1] * l1[s1][1]])
        self.syn1 = syn1

    def calc(self, delta_next, w_next, l_current, w_current, learn, l_prev):
        delta_current = [l_current[i]*(1-l_current[i])*delta_next[i]*w_next[i] for i in range(len(l_current))]
        for i in range(len(w_current)):
            w_current[i] += learn * delta_current[i] * l_prev[i]
        return w_current
